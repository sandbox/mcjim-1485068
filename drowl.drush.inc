<?php

/**
 * Implementation of hook_drush_command().
 */
function drowl_drush_command() {
  $items['growl'] = array(
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
    'description' => 'Plugin to send growl notifications.',
    'arguments' => array(
      'message' => 'The message to send via growl.',
      'title' => 'The message title.',
      'extra_params' => 'Extra parameters',
    ),
  );
  return $items;
}

/**
 * Drush callback.
 */
function drush_drowl_growl($message = '', $title = '', $extra_params = array()) {
  if (empty($message)) {
    $message = dt('No message received');
  }
  if (empty($title)) {
    $title = dt('Drush');
  }
  // Code borrowed from http://drupal.org/project/growl
  $command = '/usr/local/bin/growlnotify';
  if (!empty($message)) {
    $command .= ' -m ' . escapeshellarg($message);
  }
  if (!empty($title)) {
    $command .= ' -t ' . escapeshellarg($title);
  }
  foreach ($extra_params as $key => $value) {
    $command .= ' -' . $key . ' ' . escapeshellarg($value);
  }
  exec($command . "\n", $results);
  return $results;
}

function drowl_drush_exit() {
  $command = drush_get_command();
  $title = $command['command'];
  $log = drush_get_log();
  // drush_print_r($log);
  foreach($log as $entry) {
    if (in_array($entry['type'], array('notice', 'warning'))) {
      if (is_array($entry['message'])) {
        $output = implode("\n", $entry['message']);
      }
      else {
        $output = $entry['message'];
      }
      drush_drowl_growl($output, $title);
    }
  }  
  $output = dt("Command "). $command['command'] .dt(" completed.\n");
  $title = dt('Drush');
  drush_drowl_growl($output, $title);
}